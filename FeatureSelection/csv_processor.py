# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 15:14:56 2016

@author: imorgan.admin
"""
import re
import numpy as np
import pandas as pd

class HdSdProcessor:

    def process(df):
        tcp_stats_log_pattern = re.compile("([0-9]+?)/([0-9]+?)/([0-9]+?)\s+([0-9]+)\s+([0-9]+?)/([0-9]+?)\s+([0-9.E-]+?)/([0-9.E-]+?)$")
        media_log_items = []

        def extract_tcpstatslog(item):
            '''<minRTT>/<avgRTT>/<maxRTT>
            <BDP> <avgBIF>/<maxBIF>
            <pktLossRate>/<pktRetransRate>'''

            m = tcp_stats_log_pattern.match(item)
            if m is not None:
                vals = [int(m.group(i)) for i in range(1,7)]
                vals.extend([float(m.group(i)) for i in range(7,9)])
            else:
                vals = [np.nan for i in range(8)]
            return vals

        def extract_medialogstring(text):
            labels = ["process_id", "session_id", "input_video_properties", "optimized_video_properties", "media_time", "delay_properties",
                          "content_lengths", "input_byte_counts", "output_byte_counts", "input_flags", "output_flags", "session_flags", "jit_savings_statistics",
                          "cache_hash", "flash_version", "input_video_quantization_parameters", "first_media_timestamp", "estimated_channel_bandwidth", "cached_original_content_length",
                          "acceleration_schedule_configuration", "jit_fallback_rate", "startup_latency", "unknown"]
            media_log_items.append({labels[i]: item for i, item in enumerate(text.split(" "))})


        def numeric(df, column):
            for item in column:
                df[item] = pd.to_numeric(df[item], errors='coerce')

        def split_by(item, delimiter, func, expected = 5):
            if item == "-":
                yield np.nan

            if not isinstance(item, str):
                for i in range(expected):
                    yield np.nan
            else:
                for part in item.split(delimiter):
                    try:
                        p = func(part)
                    except:
                        p = np.nan

                    yield p

        def extract_accelerationinfo(item):
            if not isinstance(item, str):
                return [np.nan]*3

            if item == "-":
                return [np.nan]*3

            return [float(y) for y in item.split("/")[0].split(",")]

        def split_int_by_slash(item):
            return list(split_by(item, "/", lambda y: int(y)))

        def extract_flags(item, prefix, flaglist, expected):
            if isinstance(item, str):
                items = list(item)
                flaglist.append({ "{}_{}".format(prefix, i+1): v for i,v in enumerate(items) })
            else:
                flaglist.append({ "{}_{}".format(prefix, i+1): np.nan for i in range(expected)})

        # start the parsing

        (df['minrtt_ms'], df['avgrtt_ms'], df['maxrtt_ms'],
             df['bdp_kb'], df['avgbif_kb'], df['maxbif_kb'], df['pktlossrate_percent'],
                df['pktretransrate_percent']) = zip(*df.tcpstatisticslog.map(extract_tcpstatslog))

        df.drop('tcpstatisticslog', inplace=True, axis=1)
        df.medialogstring.apply(extract_medialogstring)
        df = pd.concat([pd.DataFrame(media_log_items), df], axis=1)

        df["height"], df["width"], df["fps"] = zip(*df.input_video_properties.map(lambda x: split_by(x, "x", lambda y: int(y), 3)))
        df["video_processed_ms"], df["video_dbs_ms"], df["video_advertised_duration_ms"], df["video_process_time_ms"] = zip(*df.media_time.map(split_int_by_slash))
        df["delay_property_1"], df["delay_property_2"] = zip(*df.delay_properties.map(split_int_by_slash))
        df["content_length_kb"], df["content_length_from_cache"] = zip(*df.content_lengths.map(split_int_by_slash))

        df["input_video_bytes"], df["input_audio_bytes"], df["input_overhead_bytes"], df["input_bytes_read"] = zip(*df.input_byte_counts.map(split_int_by_slash))
        df["output_video_bytes"], df["output_audio_bytes"], df["output_overhead_bytes"], df["output_bytes_sent"] = zip(*df.output_byte_counts.map(split_int_by_slash))
        df["acceleration_rate"], df["acceleration_media_time_s"], df["acceleration_transfer_time_ms"] = zip(*df.acceleration_schedule_configuration.map(extract_accelerationinfo))

        flags = []
        df.input_flags.apply(lambda x: extract_flags(x, "input_flag", flags, 3))
        df = pd.concat([df, pd.DataFrame(flags)], axis=1)

        flags = []
        df.output_flags.apply(lambda x: extract_flags(x, "output_flag", flags, 3))
        df = pd.concat([df, pd.DataFrame(flags)], axis=1)

        flags = []
        df.session_flags.apply(lambda x: extract_flags(x, "session_flag", flags, 15))
        df = pd.concat([df, pd.DataFrame(flags)], axis=1)

        df["jit_bytes_sent_during_first_burst"], df["jit_delta_first_last_ack"] = zip(*df.jit_savings_statistics.map(split_int_by_slash))
        df["average_quantization"], df["minimum_quantization"], df["maximum_quantization"], df["adaptive_quantization"] = zip(*df.input_video_quantization_parameters.map(split_int_by_slash))
        numeric(df, ["optimized_video_properties",  "first_media_timestamp", "estimated_channel_bandwidth", "cached_original_content_length", "jit_fallback_rate", "startup_latency"])

        # get rid of columns which don't change
        column_names = df.apply(lambda x: len(x.unique()) > 1)
        filtered = df[column_names[column_names == True].index.tolist()].copy()

        columns_to_delete = ["sessionid", "emsisdn", "pid", "rsize", "srsize", "clientip",  "clientport", "serverlocport", "serverip",  "redinvocation", "domain", "query", "dmy" ]
        filtered.drop(columns_to_delete, axis=1, inplace=True)

        filtered['vds'] = False
        filtered.loc[filtered["Video Download Status"] == "SUCCESS", "vds"] = True

        filtered.category.replace(to_replace='null', value=np.nan, inplace=True)
        filtered.drop(['unknown', 'session_flags', 'session_id', 'process_id', 'acceleration_schedule_configuration', 'input_video_properties', 'media_time', 'input_byte_counts', 'output_byte_counts', 'delay_properties', 'content_lengths', 'jit_savings_statistics',
                   'Video Download Status', 'mm', 'ss', 'medialogstring', 'customrepgrp', 'customrepopt1', 'customrepopt2', 'cache_hash' ], axis=1, inplace=True)

        filtered.vds = filtered.vds.astype(int)
        filtered.replace([np.inf, -np.inf], np.nan, inplace=True)

        return filtered