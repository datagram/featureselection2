# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 11:36:57 2016

@author: imorgan.admin
"""

BIVARIATE = "bivariate"
COMBINATION = "combination"
EXCLUSIVE = "exclusive"


def is_windows():
    import platform
    return platform.system() == "Windows"

def create_arg_parser():
    import argparse
    parser = argparse.ArgumentParser(description='Detect significant variables in CSV file for HD/SD video stream')
    parser.add_argument("input_file_path", help="Path to the input CSV file")
    parser.add_argument("output_file_path", help="Path to the output file")
    parser.add_argument("target",
                        help="The name of the target variable and state (separated by two dollar signs), e.g. MyVariable$$True")
    parser.add_argument("--key", "-k", help="license key for Bayes Server (not really needed at the moment).",
                        default="")
    parser.add_argument("--actions", "-a", nargs="*", default=[BIVARIATE],
                        choices=[BIVARIATE, COMBINATION, EXCLUSIVE],
                        help="""{}: the variables that have highest correlation with target variable
                                {}: the combination of variables that account for a percentage of all occurrences of the target variable
                                {}: the variables that are highly indicative of the target state, regardless of percentage of cases accounted for (strongly influenced by sample size)""".format(
                            BIVARIATE, COMBINATION, EXCLUSIVE))
    parser.add_argument("--num_models", "-n", default=3, type=int,
                        help="The number of models to train, as results can be different across models depending on the dataset")
    parser.add_argument("--top", "-t", default=5, type=int,
                        help="In ranked order, the top number of records to write to the output file.")
    parser.add_argument("--verbose", "-v", default=False, type=bool)
    return parser

def main():
    import pandas as pd
    import csv_processor
    import json
    import sys

    args = create_arg_parser().parse_args(sys.argv[1:])

    df = csv_processor.HdSdProcessor.process(pd.read_csv(args.input_file_path))

    if is_windows():
        # the module will not have been installed using pip.
        import sys
        sys.path.append("C:\\Users\\imorgan.admin\\PycharmProjects\\bayespy")

    import bayespy
    import logging

    logger = logging.getLogger('variable_selection_wrapper')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)

    auto = bayespy.data.AutoType(df)
    target = bayespy.network.Discrete.fromstring(args.target)

    db_folder = bayespy.utils.get_path_to_parent_dir(__file__)
    # create the network factory that can be used to instantiate networks
    with bayespy.network.NetworkFactory(df, db_folder, logger) as network_factory:
        # create the insight model
        insight = bayespy.insight.AutoInsight(network_factory, logger,
                                              discrete=df[list(auto.get_discrete_variables())],
                                              continuous=df[list(auto.get_continuous_variables())])
        models = insight.create_model_cache(target, times=args.num_models)

        results = {}
        if BIVARIATE in args.actions:
            results.update({BIVARIATE: insight.query_bivariate_combinations(target, models=models, top=args.top)})

        if EXCLUSIVE in args.actions:
            results.update({EXCLUSIVE: insight.query_exclusive_states(target, models=models, top=args.top)})

        if COMBINATION in args.actions:
            r, models = insight.query_top_variable_combinations(target, models=models, top=args.num_models)
            results.update(
                {COMBINATION: r})

        with open(args.output_file_path, "w") as fh:
            json.dump(results, fh)


if __name__ == "__main__":
    main()
