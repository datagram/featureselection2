from unittest import TestCase
from FeatureSelection import __main__
import nose.tools

class TestCreate_arg_parser(TestCase):

    def test_required_options(self):
        parser = __main__.create_arg_parser()

        input_file_path = "c:/test_location"
        output_file_path = "c:/output_file_location/path.json"
        target = "MyTargetVariable$$True"

        args = parser.parse_args([input_file_path, output_file_path, target])
        nose.tools.assert_equals(args.input_file_path, input_file_path)
        nose.tools.assert_equals(args.output_file_path, output_file_path)
        nose.tools.assert_equals(args.target, target)

        nose.tools.assert_list_equal(args.actions, [__main__.BIVARIATE])

    def test_actions(self):
        parser = __main__.create_arg_parser()

        input_file_path = "c:/test_location"
        output_file_path = "c:/output_file_location/path.json"
        target = "MyTargetVariable$$True"

        actions = ["--actions", __main__.BIVARIATE, __main__.COMBINATION]

        args = parser.parse_args([input_file_path, output_file_path, target] + actions)
        nose.tools.assert_list_equal(args.actions, [__main__.BIVARIATE, __main__.COMBINATION])




